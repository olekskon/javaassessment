# Java Assessment - Oleksandr Kononov

## Task One
Answer the questions from the assessment document and create a Java file to demonstrate/answer questions marked with the asterisk symbol (*).

The answers document for **Task One** will be on the Google Drive in the `Training-Assessments/Java` folder.

## Task Two
Create an Address Book application that stores all forms of contact information about a person. Unit tests should be provided to ensure all functionality works. The application should be able to do the following from the command line or IntelliJ IDEA console:
- Add/Remove/Edit a user
- List all information about a specific user
- List all users by name with the option to sort by last name or first name

Default users should be bootstrapped for testing purposes.

### Running Address Book (Using IntelliJ IDEA)
- Navigate to `Java_Assessment/src/com/errigal/oleks/` directory.
- Run `AddressBookDriver.java` class.
- A menu will be shown in the IntelliJ IDEA console which signifies that the application is running.

#### Usage
When running the **Address Book** application, the user is first presented with the _Main Menu_ screen where options are displayed in the format:

`[LETTER]. [OPTION DESCRIPTION]`

##### Example
> a. Add new user
> r. Remove existing user
> e. Edit existing user
> l. List specific user
> s. List all users
> q. Quit
> \>

Input is taken by typing the `LETTER` character into the console.

##### Example
The following will enter the *Add new user submenu*
> \> a

### Running Unit Tests
61 Unit Tests Total

- Navigate to `Java_Assessment/src/com/errigal/oleks/test/` directory.
- Open any of the test files which have the format `[CLASS NAME]Test.java`.
- Run the JUnit test file to run the test for that *CLASS NAME*.

