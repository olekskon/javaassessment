package com.errigal.oleks;
/**
 * Copyright (c) 2019 Errigal Inc.
 * <p>
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 * <p>
 * <p>
 * <p>
 * User: Oleksandr Kononov
 * Date: 2019-08-07
 */
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-07
 */

import com.errigal.oleks.controller.AddressBookController;
import com.errigal.oleks.controller.PersonController;
import com.errigal.oleks.model.Person;
import com.errigal.oleks.view.AddressBookConsoleView;
import com.errigal.oleks.view.MenuView;
import com.errigal.oleks.view.Menu;

import java.util.Comparator;
import java.util.stream.Stream;

public class AddressBookDriver {

    private PersonController controller;
    private MenuView view;
    private boolean quit;

    /**
     * Constructor for the AddressBook Driver
     *
     * Initialise the controller and view objects and populate the
     * menu operations for the view map with relevant methods which will be executed
     * on relevant menu screens.
     *
     *
     * Also bootstraps some default users.
     */
    private AddressBookDriver(){
        quit = false;
        controller = new AddressBookController();
        view = new AddressBookConsoleView();

        // Populate the menu operations for different menu screens
        view.addMenuOperation(Menu.MAIN, this::mainMenu);
        view.addMenuOperation(Menu.ADD, this::addMenu);
        view.addMenuOperation(Menu.REMOVE, this::removeMenu);
        view.addMenuOperation(Menu.EDIT, this::editMenu);
        view.addMenuOperation(Menu.LIST_USER, this::listUserMenu);
        view.addMenuOperation(Menu.LIST_USERS, this::listAllUsersMenu);

        controller.addPerson("Mr", "Bean", "1234567890", "mrbean@haha.com", "apartment");
        controller.addPerson("Rob", "OConnor", "1234509876", "rob@oconnor.ie", "NA");
        controller.addPerson("Nokia", "Phone", "0987654321", "nokia@europe.ie", "everywhere");
        controller.addPerson("Errigal", "Waterford", "0987612345", "errigal@errigal.com", "waterford");
    }

    /**
     * Main menu screen operations method which handles changing of menus for
     * the program depending on user input.
     */
    private void mainMenu(){
        String userInput = view.readInput();
        if (userInput == null){
            view.show("Unexpected input, please follow the menu!\n");
            return;
        }
        for (Menu menu : Menu.values()){
           if (userInput.equals(menu.COMMAND)) {
               view.changeMenu(menu);
               break;
           }
        }
    }

    /**
     * Add menu operations method which handles the operations for adding a new user.
     */
    private void addMenu(){
        // Gather information for new user through prompts to the view class
        String firstName = view.prompt("Enter First Name: ");
        String lastName = view.prompt("Enter Last Name: ");
        String phone = view.prompt("Enter Phone Number: ");
        String email = view.prompt("Enter Email: ");
        String address = view.prompt("Enter Address: ");

        // Construct the new Person class from gathered information
        Person newUser = new Person(firstName, lastName, phone, email, address);

        // Null report for any improper fields before adding the new user
        if (!controller.addPerson(newUser)) view.show("\nERROR\n"+newUser.nullReport());
        else view.show("New User added successfully!\n");
    }

    /**
     * Remove menu operations method which handles the operations for removing a user.
     */
    private void removeMenu(){
        // Check if there are any users available to be removed first
        if (controller.getPersonsSize() == 0){
            view.show("No Users to remove!");
            view.changeMenu(Menu.MAIN);
            return;
        }

        // List available users that can be removed and prompt user for index
        view.show(controller.toString());
        int index = view.promptInt("Select user to remove: ");

        // Attempt to remove user with given index
        if (!controller.removePerson(index)) view.show("Failed to remove user!\n");
        else view.show("Removed user successfully!\n");
    }

    /**
     * Edit menu operations method which handles the operations for editing a user.
     */
    private void editMenu(){
        // Check if there are any users available to edit
        if (controller.getPersonsSize() == 0){
            view.show("No Users to edit!");
            view.changeMenu(Menu.MAIN);
            return;
        }

        // List available users that can be edited and prompt user for index
        view.show(controller.toString());
        int index = view.promptInt("Select a user to edit: ");

        // Before gathering new information, check the user exits
        if (controller.getPerson(index) == null) {
            view.show("Wrong index selected!\n");
            return;
        }
        view.show("Enter edit data (leave blank to ignore)\n");

        String firstName = view.prompt("Enter First Name: ");
        String lastName = view.prompt("Enter Last Name: ");
        String phone = view.prompt("Enter Phone Number: ");
        String email = view.prompt("Enter Email: ");
        String address = view.prompt("Enter Address: ");

        // Attempt to edit (update) given user
        if (!controller.updatePerson(index, firstName, lastName, phone, email, address))
            view.show("Could not update person!\n");
        else view.show("Updated person successfully!\n");
    }

    /**
     * List specific user menu operations method.
     */
    private void listUserMenu(){
        // Check if there are any users available to list
        if (controller.getPersonsSize() == 0){
            view.show("No Users to list!");
            view.changeMenu(Menu.MAIN);
            return;
        }

        // List available users that can be edited and prompt user for index
        view.show(controller.toString());
        int index = view.promptInt("Select a user to view: ");

        // Attempt to get the user through index and show the data through the view class
        Person user = controller.getPerson(index);
        if (user == null) view.show("Wrong index selected!\n");
        else view.show(user.toString());
    }

    /**
     * List all users (with sorting options) menu operations method.
     */
    private void listAllUsersMenu() {
        // Check if there are any users available to list
        if (controller.getPersonsSize() == 0){
            view.show("No Users to list!");
            view.changeMenu(Menu.MAIN);
            return;
        }

        // Create streams for easy collections processing
        Stream<Person> sortedPersons;
        Stream<Person> personsStream = controller.getPersons().stream();

        // Sort all users based on soring options the user selected (first/last names)j
        switch (view.readInput()) {
            case AddressBookConsoleView.SORT_BY_FIRST:
               sortedPersons = personsStream.sorted(Comparator.comparing(Person::getFirstName));
               break;
            case AddressBookConsoleView.SORT_BY_LAST:
                sortedPersons = personsStream.sorted(Comparator.comparing(Person::getLastName));
                break;
            default:
                view.show("Unexpected input, please follow the menu!\n");
                return;
        }

        // Show each sorted users first and last name in the correct order
        sortedPersons.forEach(person -> view.show(person.getFirstName() + " " + person.getLastName()));
    }

    /**
     * Main program loop.
     */
    private void run(){
        while (!quit) view.display();
    }

    public static void main(String[] args){
        new AddressBookDriver().run();
    }
}
