package com.errigal.oleks; /**
 * Copyright (c) 2019 Errigal Inc.
 * <p>
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 * <p>
 * <p>
 * <p>
 * User: Oleksandr Kononov
 * Date: 2019-08-07
 */

import com.errigal.oleks.model.Person;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-07
 */

public class ComparatorDriver {

    public static void main(String[] args){

        /* Question S
         * Use a comparator to store person objects in a sorted set in order of their last name
         */

        SortedSet<Person> personSortedSet = new TreeSet<>(Comparator.comparing(Person::getLastName));
        personSortedSet.add(new Person("ABC", "ZXY", "1234567890", "test1@t.t", "na"));
        personSortedSet.add(new Person("BDE", "PXQ", "1234567890", "test1@t.t", "na"));
        personSortedSet.add(new Person("IOP", "ACH", "1234567890", "test1@t.t", "na"));
        personSortedSet.add(new Person("QZP", "KLM", "1234567890", "test1@t.t", "na"));
        personSortedSet.add(new Person("AGH", "BDE", "1234567890", "test1@t.t", "na"));

        personSortedSet.forEach(p -> System.out.println(p.getFirstName() + " " + p.getLastName()));
    }
}
