package com.errigal.oleks.test;

import com.errigal.oleks.controller.AddressBookController;
import com.errigal.oleks.model.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddressBookControllerTest {

    private AddressBookController controller;

    /**
     * Setup new controller instance with fresh users for each test (to avoid leakage from other tests).
     *
     * Note: Since creation of users grants a new and incremented ID, the ID cannot be known for certain for
     * each test.
     */
    @BeforeEach
    void setUp() {
        controller = new AddressBookController();
        controller.addPerson("Test1", "Tester1", "1234567890", "test1@test.com", "na1");
        controller.addPerson("Test2", "Tester2", "0987654321", "test2@test.com", "na2");
        controller.addPerson("Test3", "Tester3", "5432109876", "test3@test.com", "na3");
    }

    @Test
    void testAddPersonWithFields(){
        assertEquals(3, controller.getPersonsSize());
        assertTrue(controller.addPerson("Test4",
                                        "Tester4",
                                           "0987612345",
                                            "test4@test.com",
                                          "na4"));
        assertEquals(4, controller.getPersonsSize());

        Person newPerson = controller.getLastPerson();
        assertEquals("Test4", newPerson.getFirstName());
        assertEquals("Tester4", newPerson.getLastName());
        assertEquals("0987612345", newPerson.getPhone());
        assertEquals("test4@test.com", newPerson.getEmail());
        assertEquals("na4", newPerson.getAddress());
    }

    @Test
    void testAddPersonWithObject(){
        assertEquals(3, controller.getPersonsSize());
        Person newPerson = new Person("Test4",
                                      "Tester4",
                                        "0987612345",
                                        "test4@test.com",
                                      "na4");
        controller.addPerson(newPerson);
        assertEquals(4, controller.getPersonsSize());

        assertEquals("Test4", newPerson.getFirstName());
        assertEquals("Tester4", newPerson.getLastName());
        assertEquals("0987612345", newPerson.getPhone());
        assertEquals("test4@test.com", newPerson.getEmail());
        assertEquals("na4", newPerson.getAddress());
    }

    @Test
    void testAddPersonNullObject(){
        assertFalse(controller.addPerson(null));
        assertEquals(3, controller.getPersonsSize());
    }

    @Test
    void testAddPersonNullFields() {
        assertFalse(controller.addPerson(null, null, null, null, null));
        assertFalse(controller.addPerson("Test", null, null, null, null));
        assertFalse(controller.addPerson("Test", "Tester", null, null, null));
        assertFalse(controller.addPerson("Test", "Tester", "1234567890", null, null));
        assertFalse(controller.addPerson("Test", "Tester", "1234567890", "test@tester.com", null));
        assertEquals(3, controller.getPersonsSize());
    }

    @Test
    void testAddPersonEmptyFields(){
        assertFalse(controller.addPerson("", "", "", "",""));
        assertEquals(3, controller.getPersonsSize());
    }

    @Test
    void testRemovePersonObject(){
        Person personToRemove = controller.getLastPerson();
        long id = personToRemove.getId();

        assertEquals(3, controller.getPersonsSize());
        assertTrue(controller.removePerson(personToRemove));
        assertEquals(2, controller.getPersonsSize());
        assertNull(controller.getPerson(id));
    }

    @Test
    void testRemovePersonId(){
        long idToRemove = controller.getLastPerson().getId();

        assertEquals(3, controller.getPersonsSize());
        assertTrue(controller.removePerson(idToRemove));
        assertEquals(2, controller.getPersonsSize());
        assertNull(controller.getPerson(idToRemove));
    }

    @Test
    void testRemovePersonInvalidId(){
        assertFalse(controller.removePerson(-1));
        assertFalse(controller.removePerson(10000000));
    }

    @Test
    void testRemovePersonNull() {
        assertFalse(controller.removePerson(null));
    }

    @Test
    void testGetPerson(){
        long lastId = controller.getLastPerson().getId();
        long firstId = controller.getFirstPerson().getId();
        assertEquals(controller.getFirstPerson(), controller.getPerson(firstId));
        assertEquals(controller.getLastPerson(), controller.getPerson(lastId));
        assertSame(controller.getFirstPerson(), controller.getPerson(firstId));
        assertSame(controller.getLastPerson(), controller.getPerson(lastId));
    }

    @Test
    void testGetFirstPerson() {
        Person p = controller.getFirstPerson();
        assertEquals("Test1", p.getFirstName());
        assertEquals("Tester1", p.getLastName());
        assertEquals("1234567890", p.getPhone());
        assertEquals("test1@test.com", p.getEmail());
        assertEquals("na1", p.getAddress());
    }

    @Test
    void testGetLastPerson(){
        Person p = controller.getLastPerson();
        assertEquals("Test3", p.getFirstName());
        assertEquals("Tester3", p.getLastName());
        assertEquals("5432109876", p.getPhone());
        assertEquals("test3@test.com", p.getEmail());
        assertEquals("na3", p.getAddress());
    }

    @Test
    void testGetPersonsSize(){
        assertEquals(3, controller.getPersonsSize());
    }

    @Test
    void testGetPersonSizeAddition(){
        assertEquals(3, controller.getPersonsSize());
        assertTrue(controller.addPerson("t","tt", "1230981230", "t@t.t", "none"));
        assertEquals(4, controller.getPersonsSize());
    }

    @Test
    void testGetPersonSizeDeduction(){
        assertEquals(3, controller.getPersonsSize());
        assertTrue(controller.removePerson(controller.getLastPerson().getId()));
        assertTrue(controller.removePerson(controller.getLastPerson().getId()));
        assertEquals(1, controller.getPersonsSize());
    }

    @Test
    void testGetPersonSizeRemoveAll(){
        assertEquals(3, controller.getPersonsSize());
        assertTrue(controller.removePerson(controller.getLastPerson().getId()));
        assertTrue(controller.removePerson(controller.getLastPerson().getId()));
        assertTrue(controller.removePerson(controller.getLastPerson().getId()));
        assertEquals(0, controller.getPersonsSize());
    }

    @Test
    void testGetPersonSizeEmpty(){
        assertEquals(3, controller.getPersonsSize());
        assertTrue(controller.removePerson(controller.getLastPerson().getId()));
        assertTrue(controller.removePerson(controller.getLastPerson().getId()));

        long lastId = controller.getLastPerson().getId();
        assertTrue(controller.removePerson(controller.getLastPerson().getId()));
        assertEquals(0, controller.getPersonsSize());

        assertFalse(controller.removePerson(lastId));
        assertEquals(0, controller.getPersonsSize());
    }

    @Test
    void testUpdatePersonFirstName(){
        long id = controller.getFirstPerson().getId();
        assertEquals("Test1", controller.getPerson(id).getFirstName());
        assertTrue(controller.updatePerson(id, "Test11", null, null, null, null));
        assertEquals("Test11", controller.getPerson(id).getFirstName());
    }

    @Test
    void testUpdatePersonLastName(){
        long id = controller.getFirstPerson().getId();
        assertEquals("Tester1", controller.getPerson(id).getLastName());
        assertTrue(controller.updatePerson(id, null, "Tester11", null, null, null));
        assertEquals("Tester11", controller.getPerson(id).getLastName());
    }

    @Test
    void testUpdatePersonPhone(){
        long id = controller.getFirstPerson().getId();
        assertEquals("1234567890", controller.getPerson(id).getPhone());
        assertTrue(controller.updatePerson(id, null, null, "0987654321", null, null));
        assertEquals("0987654321", controller.getPerson(id).getPhone());
    }

    @Test
    void testUpdatePersonEmail(){
        long id = controller.getFirstPerson().getId();
        assertEquals("test1@test.com", controller.getPerson(id).getEmail());
        assertTrue(controller.updatePerson(id, null, null, null, "test11@test.com", null));
        assertEquals("test11@test.com", controller.getPerson(id).getEmail());
    }

    @Test
    void testUpdatePersonAddress() {
        long id = controller.getFirstPerson().getId();
        assertEquals("na1", controller.getPerson(id).getAddress());
        assertTrue(controller.updatePerson(id, null, null, null, null, "na11"));
        assertEquals("na11", controller.getPerson(id).getAddress());
    }

    /**
     * Test updating all fields for a user.
     */
    @Test
    void testUpdatePersonEverything(){
        long id = controller.getFirstPerson().getId();

        assertEquals("Test1", controller.getPerson(id).getFirstName());
        assertEquals("Tester1", controller.getPerson(id).getLastName());
        assertEquals("1234567890", controller.getPerson(id).getPhone());
        assertEquals("test1@test.com", controller.getPerson(id).getEmail());
        assertEquals("na1", controller.getPerson(id).getAddress());

        assertTrue(controller.updatePerson(id,
                                            "UpdatedTest1",
                                            "UpdatedTester1",
                                            "0987654321",
                                            "updatedTest1@test.com",
                                          "new address"));

        assertEquals("UpdatedTest1", controller.getPerson(id).getFirstName());
        assertEquals("UpdatedTester1", controller.getPerson(id).getLastName());
        assertEquals("0987654321", controller.getPerson(id).getPhone());
        assertEquals("updatedTest1@test.com", controller.getPerson(id).getEmail());
        assertEquals("new address", controller.getPerson(id).getAddress());
    }
}