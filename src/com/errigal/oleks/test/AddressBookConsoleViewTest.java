package com.errigal.oleks.test;

import com.errigal.oleks.view.AddressBookConsoleView;
import com.errigal.oleks.view.Menu;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class AddressBookConsoleViewTest {

    /* NOTE:
     * Due to issues with setting a local input stream similar to the outContent (from ByteArrayInputStream),
     * there are no tests for prompt, readInput and promptInt as they require user input.
     *
     * Presetting test input did not succeed.
     */

    // Change output stream so we can see what's being printed out
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    private AddressBookConsoleView view;

    @BeforeEach
    void setUp(){
        System.setOut(new PrintStream(outContent));
        view = new AddressBookConsoleView();
    }

    @AfterEach
    void tearDown(){
        System.setOut(originalOut);
    }

    @Test
    void testDisplay() {
        // Display Main Menu
        String expectedMenuString = "ADDRESS BOOK MENU\n\n" +
                Menu.ADD.COMMAND+". Add new user\n" +
                Menu.REMOVE.COMMAND+". Remove existing user\n" +
                Menu.EDIT.COMMAND+". Edit existing user\n" +
                Menu.LIST_USER.COMMAND+". List specific user\n" +
                Menu.LIST_USERS.COMMAND+". List all users\n" +
                Menu.QUIT.COMMAND+". Quit\n"+
                "> ";
        view.display();
        assertEquals(expectedMenuString, outContent.toString());
    }

    /**
     * Test valid show method.
     */
    @Test
    void testShow(){
        view.show("Testing");
        assertEquals("Testing\n", outContent.toString());
    }

    /**
     * Test empty String show method.
     */
    @Test
    void testShowEmpty(){
        view.show("");
        assertEquals("", outContent.toString());
    }

    /**
     * Test null show method.
     */
    @Test
    void testShowNull(){
        view.show(null);
        assertEquals("", outContent.toString());
    }

    /**
     * Test no operations present for addMenuOperations method.
     */
    @Test
    void testAddMenuOperationNoOperations() {
        String expectedMenuString = "ADDRESS BOOK MENU\n\n" +
                Menu.ADD.COMMAND+". Add new user\n" +
                Menu.REMOVE.COMMAND+". Remove existing user\n" +
                Menu.EDIT.COMMAND+". Edit existing user\n" +
                Menu.LIST_USER.COMMAND+". List specific user\n" +
                Menu.LIST_USERS.COMMAND+". List all users\n" +
                Menu.QUIT.COMMAND+". Quit\n"+
                "> ";

        view.display();
        assertEquals(expectedMenuString, outContent.toString());
    }

    @Test
    void testAddMenuOperationWithOperations() {
        String expectedMenuString = "ADDRESS BOOK MENU\n\n" +
                Menu.ADD.COMMAND+". Add new user\n" +
                Menu.REMOVE.COMMAND+". Remove existing user\n" +
                Menu.EDIT.COMMAND+". Edit existing user\n" +
                Menu.LIST_USER.COMMAND+". List specific user\n" +
                Menu.LIST_USERS.COMMAND+". List all users\n" +
                Menu.QUIT.COMMAND+". Quit\n"+
                "> ";
        String testListOperation = "list menu operation";

        view.addMenuOperation(Menu.MAIN, () -> System.out.print(testListOperation));
        view.display();
        assertEquals(expectedMenuString+testListOperation, outContent.toString());
    }

    @Test
    void testChangeGetMenu(){
        assertEquals(Menu.MAIN, view.getCurrentMenu());
        view.changeMenu(Menu.ADD);
        assertEquals(Menu.ADD, view.getCurrentMenu());
    }

}