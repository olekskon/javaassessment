package com.errigal.oleks.test;

import static org.junit.jupiter.api.Assertions.*;

import com.errigal.oleks.model.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PersonTest {

    private Person p;

    @BeforeEach
    void setUp() {
        p = new Person("Test", "Tester", "353812345678", "tester@test.test", "none");
    }

    /**
     * Tests Person constructor for valid fields
     */
    @Test
    void testConstructorValid(){
        Person person = new Person("T1", "T2", "1234567890", "mail@mail.com", "none");
        assertEquals("T1", person.getFirstName());
        assertEquals("T2", person.getLastName());
        assertEquals("1234567890", person.getPhone());
        assertEquals("mail@mail.com", person.getEmail());
        assertEquals("none", person.getAddress());

        // Null report is empty only if all fields are set
        assertEquals("", person.nullReport());
    }

    /**
     * Tests Person constructor for null fields
     */
    @Test
    void testConstructorNull(){
        Person person = new Person(null, null, null, null, null);
        assertNull(person.getFirstName());
        assertNull(person.getLastName());
        assertNull(person.getPhone());
        assertNull(person.getEmail());
        assertNull(person.getAddress());

        // Null report shows all fields are not set
        assertEquals("First Name is not set!\n" +
                     "Last Name is not set!\n" +
                     "Phone Number is not set!\n" +
                     "Email is not set!\n" +
                     "Address is not set!\n",
                     person.nullReport());
    }

    @Test
    void testGetFirstName() {
        assertEquals("Test", p.getFirstName());
    }

    /**
     * Test valid setFirstName method.
     */
    @Test
    void testSetFirstName(){
        assertEquals("Test", p.getFirstName());
        p.setFirstName("NotTest");
        assertEquals("NotTest", p.getFirstName());
    }

    /**
     * Test null setFirstName method.
     */
    @Test
    void testSetFirstNameNull(){
        p.setFirstName(null);
        assertEquals("Test", p.getFirstName());
    }

    /**
     * Test empty string setFirstName method.
     */
    @Test
    void testSetFirstNameEmpty(){
        p.setFirstName("");
        assertEquals("Test", p.getFirstName());
    }

    /**
     * Test white space setFirstName method.
     */
    @Test
    void testSetFirstNameWhitespace(){
        p.setFirstName("       ");
        assertEquals("Test", p.getFirstName());
    }

    @Test
    void testGetLastName() {
        assertEquals("Tester", p.getLastName());
    }

    /**
     * Test valid setLastName method.
     */
    @Test
    void testSetLastName(){
        assertEquals("Tester", p.getLastName());
        p.setLastName("NotTester");
        assertEquals("NotTester", p.getLastName());
    }

    /**
     * Test null setLastName method.
     */
    @Test
    void testSetLastNameNull(){
        p.setLastName(null);
        assertEquals("Tester", p.getLastName());
    }

    /**
     * Test empty string setLastName method.
     */
    @Test
    void testSetLastNameEmpty(){
        p.setLastName("");
        assertEquals("Tester", p.getLastName());
    }

    /**
     * Test white space setLastName method.
     */
    @Test
    void testSetLastNameWhitespace(){
        p.setLastName("       ");
        assertEquals("Tester", p.getLastName());
    }

    @Test
    void testGetPhone() {
        assertEquals("353812345678", p.getPhone());
    }

    /**
     * Test valid setPhone method.
     */
    @Test
    void testSetPhone(){
        assertEquals("353812345678", p.getPhone());
        p.setPhone("1234567890");
        assertEquals("1234567890", p.getPhone());
    }

    /**
     * Test null setPhone method.
     */
    @Test
    void testSetPhoneNull(){
        p.setPhone(null);
        assertEquals("353812345678", p.getPhone());
    }

    /**
     * Test empty setPhone method.
     */
    @Test
    void testSetPhoneEmpty(){
        p.setPhone("");
        assertEquals("353812345678", p.getPhone());
    }

    /**
     * Test white space setPhone method.
     */
    @Test
    void testSetPhoneWhitespace(){
        p.setPhone("      ");
        assertEquals("353812345678", p.getPhone());
    }

    /**
     * Test short number setPhone method.
     */
    @Test
    void testSetPhoneShort(){
        p.setPhone("1234");
        assertEquals("353812345678", p.getPhone());
    }

    /**
     * Test no digit characters setPhone method.
     */
    @Test
    void testSetPhoneNoDigit() {
        p.setPhone("abcdefg123456");
        assertEquals("353812345678", p.getPhone());
    }

    @Test
    void testGetEmail() {
        assertEquals("tester@test.test", p.getEmail());
    }

    /**
     * Test valid setEmail method.
     */
    @Test
    void testSetEmail(){
        assertEquals("tester@test.test", p.getEmail());
        p.setEmail("mail@mail.com");
        assertEquals("mail@mail.com", p.getEmail());
    }

    /**
     * Test null setEmail method.
     */
    @Test
    void testSetEmailNull(){
        p.setEmail(null);
        assertEquals("tester@test.test", p.getEmail());
    }

    /**
     * Test empty setEmail method.
     */
    @Test
    void testSetEmailEmpty(){
        p.setEmail("");
        assertEquals("tester@test.test", p.getEmail());
    }

    /**
     * Test white space setEmail method.
     */
    @Test
    void testSetEmailWhitespace(){
        p.setEmail("      ");
        assertEquals("tester@test.test", p.getEmail());
    }

    /**
     * Test wrong format setEmail method.
     */
    @Test
    void testSetEmailFormatting() {
        // Verify email format without proper '@'
        p.setEmail("testtester.test");
        p.setEmail("test.tester@test");
        assertEquals("tester@test.test", p.getEmail());

        // Verify email format without proper'.'
        p.setEmail("testtester@test");
        p.setEmail("test.tester@test");
        assertEquals("tester@test.test", p.getEmail());

        // Verify email format 'no user'
        p.setEmail("@test.com");
        assertEquals("tester@test.test", p.getEmail());

        // Verify email format 'improper domain name'
        p.setEmail("testtester@.com.com");
        assertEquals("tester@test.test", p.getEmail());
    }

    @Test
    void testGetAddress() {
        assertEquals("none", p.getAddress());
    }

    /**
     * Test valid setAddress method.
     */
    @Test
    void testSetAddresss(){
        assertEquals("none", p.getAddress());
        p.setAddress("8 nowhere, nowhere");
        assertEquals("8 nowhere, nowhere", p.getAddress());
    }

    /**
     * Test null setAddresss method.
     */
    @Test
    void testSetAddressNull(){
        assertEquals("none", p.getAddress());
        p.setAddress(null);
        assertEquals("none", p.getAddress());
    }

    /**
     * Test empty String setAddress method.
     */
    @Test
    void testSetAddressEmpty(){
        assertEquals("none", p.getAddress());
        p.setAddress("");
        assertEquals("none", p.getAddress());
    }

    /**
     * Test white space setAddress method.
     */
    @Test
    void testSetAddressWhitespace(){
        assertEquals("none", p.getAddress());
        p.setAddress("      ");
        assertEquals("none", p.getAddress());
    }

    @Test
    void testToString() {
        String expected = "First Name: Test\n" +
                          "Last Name: Tester\n" +
                          "Phone Number: 353812345678\n"+
                          "Email: tester@test.test\n" +
                          "Address: none\n";
        assertEquals(expected, p.toString());
    }
}