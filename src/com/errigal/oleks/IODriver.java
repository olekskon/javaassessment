package com.errigal.oleks; /***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 06-08-2019
 */

import java.io.*;
import java.util.Objects;
import java.util.Scanner;

public class IODriver {

    /**
     * Question H.
     * Function to write to a given file, will constantly write text until
     * a blank line is given (since no specific requirements were specified)
     * @param file
     * @throws IOException
     */
    private static void write(File file) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))){
            Scanner in = new Scanner(System.in);
            String cur;
            while(!(cur = in.nextLine()).isBlank()) bw.write(cur);
            in.close();
        }
    }

    /**
     * Question I.
     * Function to read from a given file
     * @param file
     * @throws IOException
     */
    private static void read(File file) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(file))){
            String cur;
           while((cur = br.readLine()) != null) System.out.println(cur);
        }
    }

    /**
     * Question J.
     * Function to read file names of a given directory
     * @param file
     */
    private static void dir(File file){
        if (file == null || !file.exists()) return;
        if (!file.isDirectory()) {
            System.out.println("Path given in not a directory!");
            return;
        }

        for(File f : Objects.requireNonNull(file.listFiles())) System.out.println(f.getName());

    }

    /**
     * Question K
     * Function to recursively print the file names starting from given directory
     * @param file
     */
    private static void dirRecursive(File file){
        if (file == null || !file.exists()) return;
        if (file.isFile()) {
            System.out.println(file.getName());
            return;
        }

        for(File f : Objects.requireNonNull(file.listFiles())) dirRecursive(f);
    }

    public static void main(String[] args) throws IOException {
        File file;
        if (args.length <= 1) {
            System.out.println("Please specify a file for IO and function");
            System.exit(1);
        }

        file = new File(args[0]);

        switch (args[1]){
            case "w":
                if (file.isFile() && file.canWrite()) write(file);
                else System.out.println("Is not a file or wrong permissions for write");
                break;
            case "r":
                if (file.isFile() && file.canRead()) read(file);
                else System.out.println("Is not a file or wrong permissions for read");
                break;
            case "d":
                dir(file);
                break;
            case "dr":
                dirRecursive(file);
                break;
        }
    }
}
