/**
 * Copyright (c) 2019 Errigal Inc.
 * <p>
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 * <p>
 * <p>
 * <p>
 * User: Oleksandr Kononov
 * Date: 2019-08-07
 */
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-07
 */

package com.errigal.oleks.view;

public enum Menu {
    MAIN(""),
    ADD("a"),
    REMOVE("r"),
    EDIT("e"),
    LIST_USER("l"),
    LIST_USERS("s"),
    QUIT("q");

    public final String COMMAND;

    Menu(String command){
        this.COMMAND = command;
    }
}
