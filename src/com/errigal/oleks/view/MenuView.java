/**
 * Copyright (c) 2019 Errigal Inc.
 * <p>
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 * <p>
 * <p>
 * <p>
 * User: Oleksandr Kononov
 * Date: 2019-08-07
 */
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-07
 */

package com.errigal.oleks.view;

public interface MenuView {

    /**
     * Main display to user, used for menus.
     */
    void display();

    /**
     * Show some information to user.
     * @param data String data to show to user
     */
    void show(String data);

    /**
     * Add menu event handling through runnable operations.
     * @param forMenu Menu enum state
     * @param operation Runnable operation to execute for given menu state
     */
    void addMenuOperation(Menu forMenu, Runnable operation);

    /**
     * Change current menu state to given menu.
     * @param menu Menu enum state
     */
    void changeMenu(Menu menu);

    /**
     * Get current menu state.
     * @return Menu enum state
     */
    Menu getCurrentMenu();

    /**
     * Directly read input from user.
     * @return String input from user
     */
    String readInput();

    /**
     * Prompt user for String input using message.
     * @param message String prompt
     * @return String user input
     */
    String prompt(String message);

    /**
     * Prompt user for int input using message.
     * @param message String prompt
     * @return int user input
     */
    int promptInt(String message);
}
