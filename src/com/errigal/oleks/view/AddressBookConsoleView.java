/**
 * Copyright (c) 2019 Errigal Inc.
 * <p>
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 * <p>
 * <p>
 * <p>
 * User: Oleksandr Kononov
 * Date: 2019-08-07
 */
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-07
 */

package com.errigal.oleks.view;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

public class AddressBookConsoleView implements MenuView {

    /* List Users Menu Options */
    public static final String SORT_BY_FIRST = "f";
    public static final String SORT_BY_LAST = "l";

    /* Map containing runnable operations for different menu options */
    private HashMap<Menu, Runnable> menuOperations;
    private Menu currentMenu;
    private Scanner in;

    public AddressBookConsoleView(){
        currentMenu = Menu.MAIN;
        menuOperations = new HashMap<>();
        in = new Scanner(System.in);
    }

    /**
     * Depending on currentMenu state, display corresponding menu in the view (console)
     * and attempt to run the associated operation with the current menu.
     */
    @Override
    public void display() {
        switch (currentMenu) {
            case MAIN:
                displayMainMenu();
                break;
            case ADD:
                displayAddMenu();
                break;
            case REMOVE:
                displayRemoveMenu();
                break;
            case EDIT:
                displayEditMenu();
                break;
            case LIST_USER:
                displayListUserMenu();
                break;
            case LIST_USERS:
                displayListUsersMenu();
                break;
        }

        Menu menuPreChange = currentMenu;

        // Check if the current menu has a runnable operation associated with it, if so, run it
        if (menuOperations.containsKey(currentMenu)) menuOperations.get(currentMenu).run();

        // Prompt user to return to main menu from current (non main) menu
        if (menuPreChange != Menu.MAIN){
            String editMore = prompt("Go back to main menu?(y/n): ");
            if (editMore.equals("y") || editMore.equals("Y")) currentMenu = Menu.MAIN;
        }
    }

    @Override
    public void show(String data){
        if (data != null && !data.isBlank()) System.out.println(data);
    }

    /**
     * Add an operation association with a given menu.
     * @param forMenu Menu enum
     * @param operation Runnable operation
     */
    @Override
    public void addMenuOperation(Menu forMenu, Runnable operation) {
        menuOperations.put(forMenu, operation);
    }

    /**
     * Display the Main Menu to the view (console).
     */
    private void displayMainMenu(){
       String menuMain = "ADDRESS BOOK MENU\n\n" +
                Menu.ADD.COMMAND+". Add new user\n" +
                Menu.REMOVE.COMMAND+". Remove existing user\n" +
                Menu.EDIT.COMMAND+". Edit existing user\n" +
                Menu.LIST_USER.COMMAND+". List specific user\n" +
                Menu.LIST_USERS.COMMAND+". List all users\n" +
                Menu.QUIT.COMMAND+". Quit\n"+
                "> ";
        System.out.print(menuMain);
    }

    /**
     * Display the List Users Menu to the view (console).
     */
    private void displayListUsersMenu() {
        System.out.print("\nLIST ALL USERS MENU\n" +
                SORT_BY_FIRST+". Sort all users by first name\n" +
                SORT_BY_LAST+". Sort all users by last name\n" +
                "> ");
    }

    /**
     * Display the List User Menu to the view (console).
     */
    private void displayListUserMenu() {
        System.out.println("\nLIST USER MENU\n");
    }

    /**
     * Display the Edit Menu to the view (console).
     */
    private void displayEditMenu() {
        System.out.println("\nEDIT USER MENU\n");
    }

    /**
     * Display the Remove Menu to the view (console).
     */
    private void displayRemoveMenu() {
        System.out.println("\nREMOVE USER MENU\n");
    }

    /**
     * Display the Add Menu to the view (console).
     */
    private void displayAddMenu() {
        System.out.println("\nADD NEW USER MENU\n");
    }

    /**
     * Change current menu state.
     * @param menu Menu enum
     */
    @Override
    public void changeMenu(Menu menu) {
        currentMenu = menu;
    }

    /**
     * Get current menu state.
     * @return Menu enum state
     */
    @Override
    public Menu getCurrentMenu(){
        return currentMenu;
    }

    /**
     * Read user input from stdin.
     * @return String of user input
     */
    @Override
    public String readInput() {
        return in.nextLine();
    }

    /**
     * Prompt user for input with message.
     * @param message String to prompt
     * @return String user input
     */
    @Override
    public String prompt(String message){
        if (message != null) System.out.print(message);
        return in.nextLine();
    }

    /**
     * Prompt user for numeric input with message.
     * @param message String to prompt
     * @return int user input, -1 if unsuccessful
     */
    @Override
    public int promptInt(String message){
        if (message != null) System.out.print(message);
        try{
            int i = in.nextInt();
            in.nextLine(); // Flush scanner
            return i;
        } catch (InputMismatchException e){
            System.out.println("Please enter an integer!");
            return -1;
        }
    }
}
