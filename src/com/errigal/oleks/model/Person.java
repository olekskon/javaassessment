/**
 * Copyright (c) 2019 Errigal Inc.
 * <p>
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 * <p>
 * <p>
 * <p>
 * User: Oleksandr Kononov
 * Date: 07-08-2019
 */
/**
 *
 * User: Oleksandr Kononov
 * Date: 07-08-2019
 */

package com.errigal.oleks.model;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Person class containing addressing information of a person.
 */
public class Person {

    /* Thread safe id counter for each new person's ID */
    private static final AtomicInteger idCounter = new AtomicInteger(0);

    private final long id;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private String address;

    public Person(String firstName, String lastName, String phone, String email, String address) {
        setFirstName(firstName);
        setLastName(lastName);
        setPhone(phone);
        setEmail(email);
        setAddress(address);
        id = idCounter.incrementAndGet();
    }

    public long getId(){
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName == null) return;
        if (firstName.isBlank()) return;
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName == null) return;
        if (lastName.isBlank()) return;
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        // Pre setting checks with regex match check for phone number format
        if (phone == null) return;
        if (phone.isBlank()) return;
        if (phone.length() <= 9) return;
        if (!phone.matches("\\+?[0-9]+")) return;
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        // Pre setting checks with regex match check for email format
        if (email == null) return;
        if (email.isBlank()) return;
        if (!email.matches("([a-zA-Z0-9]+)@([a-zA-Z0-9]+)\\.([a-zA-Z0-9]+)")) return;
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        if (address == null) return;
        if (address.isBlank()) return;
        this.address = address;
    }

    /**
     * Checks if any of the field are null and reports them.
     * @return String report of null fields
     */
    public String nullReport(){
        String report = "";
        if (firstName == null) report += "First Name is not set!\n";
        if (lastName == null) report += "Last Name is not set!\n";
        if (phone == null) report += "Phone Number is not set!\n";
        if (email == null) report += "Email is not set!\n";
        if (address == null) report += "Address is not set!\n";
        return report;
    }

    @Override
    public String toString(){
        String s = "";
        s += "First Name: "+firstName+"\n";
        s += "Last Name: "+lastName+"\n";
        s += "Phone Number: "+phone+"\n";
        s += "Email: "+email+"\n";
        s += "Address: "+address+"\n";
        return s;
    }

    /**
     * Auto generated equals override method for comparing Person objects by their ID.
     * @param o Another object to compare to
     * @return boolean value of equality
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id;
    }

    /**
     * Auto generated hashCode method for Person class based on Person ID.
     * @return int hash value
     */
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
