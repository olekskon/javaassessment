package com.errigal.oleks; /***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 06-08-2019
 */

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateDriver {

    public static void main(String[] args) {
        /* Question F
         * Create a date that represents today and print it out into the console in the format
         * "2015-May-25 23:11:59"
         */
        Format formatter = new SimpleDateFormat("yyyy-MMMM-dd HH:MM:ss");
        Date today = new Date(System.currentTimeMillis());
        System.out.println(formatter.format(today));

        /* Question G
         * Add a day to this date (today) and print it out to console
         */
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.add(Calendar.DATE, 1);
        System.out.println(formatter.format(calendar.getTime()));
    }
}
