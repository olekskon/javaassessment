/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 07-08-2019
 */

package com.errigal.oleks.controller;

import com.errigal.oleks.model.Person;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class AddressBookController implements PersonController {

    private List<Person> personList;

    public AddressBookController(){
        personList = new ArrayList<>();
    }

    /**
     * Adds person object to personList collection.
     * @param person to be added
     * @return boolean status of success
     */
    @Override
    public boolean addPerson(Person person) {
        if (person == null) {
            return false;
        }
        if (!person.nullReport().isBlank()) {
            return false;
        }
        return personList.add(person);
    }

    /**
     * Creates and adds person object to the personList collection.
     * @param firstName persons first name
     * @param lastName persons last name
     * @param phone persons phone number
     * @param email persons email address
     * @param address persons address
     * @return boolean status of success
     */
    @Override
    public boolean addPerson(String firstName, String lastName, String phone, String email, String address) {
        return addPerson(new Person(firstName, lastName, phone, email, address));
    }

    /**
     * Removes a person from the personList collection using their index.
     * @param id of the person to be removed
     * @return boolean status of success
     */
    @Override
    public boolean removePerson(long id) {
        if (personList.isEmpty()) {
            return false;
        }
        return removePerson(getPerson(id));
    }

    /**
     * Removes a person from the personList collection.
     * @param person object to be removed
     * @return boolean status of success
     */
    @Override
    public boolean removePerson(Person person) {
        if (person == null) {
            return false;
        }
        return personList.remove(person);
    }

    /**
     * Gets person object from personList collection using index.
     * @param id of the person to be returned from the address book collection
     * @return person object
     */
    @Override
    public Person getPerson(long id) {
        if (personList.isEmpty()) {
            return null;
        }
        Optional<Person> person =  personList
                                        .stream()
                                        .parallel()
                                        .filter(p -> p.getId() == id)
                                        .findFirst();
        return person.orElse(null);
    }

    /**
     * Gets last person added to the personList.
     * @return person object
     */
    @Override
    public Person getLastPerson(){
        if (personList.isEmpty()) {
            return null;
        }
        return personList
                .stream()
                .max(Comparator.comparing(Person::getId))
                .get();
    }

    /**
     * Get first person added to the personList.
     * @return
     */
    @Override
    public Person getFirstPerson(){
        if (personList.isEmpty()) {
            return null;
        }
        return personList
                .stream()
                .min(Comparator.comparing(Person::getId))
                .get();
    }

    /**
     * Get size of the personList collection.
     * @return
     */
    @Override
    public int getPersonsSize() {
        return personList.size();
    }

    /**
     * Get personList collection.
     * @return list of person objects
     */
    @Override
    public List<Person> getPersons(){
        return personList;
    }

    /**
     * Updates information of a person object from the peronsList using index.
     * @param id of the person to be updated
     * @param firstName String first name
     * @param lastName String last name
     * @param phone String phone number
     * @param email String email addresss
     * @param address String address
     * @return boolean success status
     */
    @Override
    public boolean updatePerson(long id, String firstName, String lastName, String phone, String email, String address) {
        Person personToEdit = getPerson(id);
        if (personToEdit == null){
            return false;
        }
        String currentPersonData = personToEdit.toString();
        personToEdit.setFirstName(firstName);
        personToEdit.setLastName(lastName);
        personToEdit.setPhone(phone);
        personToEdit.setEmail(email);
        personToEdit.setAddress(address);
        return !personToEdit.toString().equals(currentPersonData);
    }

    /**
     * Prints all person objects first and last names with corresponding index of the personList collection.
     * @return formatted string
     */
    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        for(Person p : personList) {
            result.append(p.getId())
                    .append(": ")
                    .append(p.getFirstName())
                    .append(" ")
                    .append(p.getLastName())
                    .append("\n");
        }
        return result.toString();
    }
}
